<?php

if(isset($_POST['enviar'])){
    
$nome       = $_POST["nome"];
$email      = $_POST["email"];
$telefone   = $_POST['fone'];
$assunto    = $_POST["assunto"];
$conteudo   = $_POST["mensagem"];

$para = 'contatos@arizonabanda.com.br';  

// Definindo o aspecto da mensagem



$mensagem = '<html>
    <head>
        <title>Mensagem</title>
    </head>';

$mensagem .= '<body>';

$mensagem .= 'Nome: '.$nome.'';
$mensagem .= '<br>';

$mensagem .= 'E-Mail: '.$email.'';
$mensagem .= '<br>';

$mensagem .= 'Fone: '.$telefone.'';
$mensagem .= '<br>';

$mensagem .= 'Assunto: '.$assunto.'';
$mensagem .= '<br>';

$mensagem .= 'Mensagem: '.$conteudo.'';
$mensagem .= '<br>';

$mensagem .= '</body>';
$mensagem .= '</html>';

// To send HTML mail, the Content-type header must be set
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= 'From: Conta <'.$email.'>' . "\r\n";

mail($para, $assunto, $mensagem, $headers);
 
}


?>
<html>
    <head>
        <title>Banda Arizona</title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Musicas Evangelicas, arizonabanda, Imperatriz - MA, Baixe nosso CD (LONGE ANDEI)...">
        <meta name="robots" content="index,follow">
        <meta http-equiv="content-language" content="pt-br">
        <meta name="author" content="7Núcleos">
        <meta name="reply-to" content="contato@arizonabanda.com.br">
        <script src="script/prefixfree.min.js"></script>
        <script src="script/arizonaMascara.js"></script>
        <script src="script/arizona.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Playfair+Display+SC' rel='stylesheet' type='text/css'>
        
        <link rel="stylesheet" href="style/normalize.css">
        <link rel="stylesheet" href="style/Arizona.css">
        
        <!--Slide-->
        <script src="slide/sliderengine/jquery.js"></script>
        <script src="slide/sliderengine/amazingslider.js"></script>
        <script src="slide/sliderengine/initslider-1.js"></script>
        <!--Slide Fim-->

    </head>
    <body>
        <?php
        
        ?>
        <div class="topo">
                
                <div class="menu-logo">
                    <img src="img/top_arizona.png" alt="Arizona">
                </div>
                
                    
                    <nav>
                        <ul>
                            <li>
                                <a href="#home">Home</a>
                            </li>
                            
                                <li>
                                    <a href="#release">Release</a>
                                </li>

                            <li>
                                <a href="#discografia">Discografia</a>
                            </li>

                                <li>
                                   <a href="#videos">Vídeos</a> 
                                </li>

                            <li>
                                <a href="#imagens">Fotos</a>
                            </li>

                                <li>
                                    <a href="#agenda">Agenda</a>
                                </li>

                            <li>
                                <a href="#contato">Contato</a>
                            </li>
                        </ul>   
                    </nav>
                
        </div>
        
        
        <div class="content-pr">
                
                <section id="home">
                    <article style="width: 100%;"  class="view-capa capa"> <!--Renderizar a pagina-->
                        <div class="main-video">
                            <iframe width="560" height="315" src="http://www.youtube.com/embed/190G0cYjIqY" frameborder="0" allowfullscreen></iframe><br>
                            <h4>Clique abaixo para baixar a música <b style="color: red;">BREVE NOVO CLIP DA BANDA</b></h4>
                            <a href="audio/LONGE_ANDEI_ARIOZNA.rar"><img src="img/baixaMusicaD.png" alt="Baixa Musica"></a>
                        </div>
                        
                        <div class="social">
                            <ul>
                                <li>
                                    <a href="https://www.facebook.com/arizonabanda?ref=ts&fref=ts"><img src="img/social/facebook.png" alt="facebook"></a>
                                </li>
                                
                                    <li>
                                        <a href="https://twitter.com/ArizonaBanda"><img src="img/social/twitter.png" alt="twitter"></a>
                                    </li>
                                    
                                    <li>
                                        <a href="http://www.youtube.com/user/arizonabanda"><img src="img/social/youtube.png" alt="youtube"></a>
                                    </li>
                                    
                                    <li>
                                        <a href="https://soundcloud.com/arizonabanda/sets"><img src="img/social/soundcloud-icon.png" alt="Sound Cloud" title="Sound Cloud"></a>
                                    </li>
                                
                                <li>
                                    <a href="http://instagram.com/arizonabanda"><img src="img/social/instagram.png" alt="instagram"></a>
                                </li>
                            </ul>
                        </div>
                    </article>
                    <div class="clear"></div>
                </section>
        
                <section class="release" id="release">
                        <article>
                            <div class="historico">
                                
                                <header>
                                    <b>História da banda</b>
                                </header>
                                
                                <span class="space-null">
                                    A banda foi fundada em 2004 pelo então baterista Tiago Rodrigues (fininho), na época faziam cover de grandes bandas de rock do cenário nacional, como: Oficina g3, Katsbarnea, Resgate e militantes. No início o nome era Supremacia.<br><br>
                                    Em 2005 começaram a tocar composições próprias no estilo 'pop rock', três anos mais tarde houve algumas mudanças e a banda passou a abraçar o estilo musical ''metal progressivo'', porém o publico não assimilou bem a proposta musical da banda, fazendo com que posteriormente viesse  a mudar novamente o estilo.<br><br>
                                    No segundo semestre de 2013 a banda passou por uma reformulação estrutural, tendo como consequência a mudança do nome, de Supremacia para Arizona.<br><br>
                                    Arizona então foi um marco para a banda, tendo como título do seu primeiro trabalho: "Longe Andei", gravado em 2013  com músicas que contam experiências próprias dos integrantes, que falam de fé e amor vividas ao longo de suas vidas.
                                </span>
                                
                                <img src="img/componentes/arizona2014.png" alt="Componentes Arizona">
                                
                                <span>
                                    <b>Componentes da Banda Arizona</b>
                                </span>
                                
                            </div>

                        </article>
                    
                        <article class="size-view">
                            <div class="historico">
                                <p>
                                    <span>
                                        <b>Thiago Cirqueira</b><br><br>
                                        
                                        Thiago Cirqueira da Silva, natural de Imperatriz - MA,vocalista da banda de rock Arizona, músico Cristão membro da Assembleia de Deus "Lírio dos Vales" de Imperatriz - MA.
                                        Começou a carreira ministerial aos 10 anos de idade cantando nos grupos de louvores da igreja, está com a banda desde a fundação(2004).
                                    </span>
                                    
                                    <img src="img/componentes/thyago_cirqueira.png" alt="Thyago Cirqueira">
                                </p>
                                
                                    <p>
                                        <span class="clear">
                                            <b>Jônatas Silva</b><br><br>
                                            
                                            Jônatas Silva, natural de Imperatriz - MA,guitarrista da banda de rock Arizona,
                                            músico Cristão, frequenta a igreja Batista Missionária em Imperatriz - MA.
                                            Começou a tocar violão aos 15 anos de idade, está com a banda desde 2010.
                                        </span>
                                        
                                        <img src="img/componentes/jonatas.png" alt="Jônatas Silva">
                                    </p>
                                    
                                <p>
                                    <span class="clear">
                                        <b>Hudson Cirqueira</b><br><br>
                                        
                                        Hudson Cirqueira da Silva, natural de Imperatriz - MA,guitarrista de rock da banda Arizona, músico Cristão membro da Assembleia de Deus "Lírio dos Vales" de Imperatriz - MA.
                                        Começou a tocar violão aos 14 anos de idade, está com a banda desde a fundação(2004).
                                    </span>
                                    
                                    <img src="img/componentes/hudson.png" alt="Hudson Cirqueira">
                                </p>
                                
                                    <p>
                                        <span class="clear">
                                            <b>Thiago Silva</b><br><br>
                                            
                                            Thiago Silva Rodrigues, natural de Imperatriz - MA, baterista da banda de rock Arizona,
                                            músico Cristão frequenta a igreja Assembleia de Deus em Imperatriz - MA.
                                            Começou a tocar bateria  aos 14 anos de idade, está com a banda desde a fundação(2004).
                                        </span>
                                        
                                        <img src="img/componentes/thyago_silva.png" alt="Thiago Silva">
                                    </p>
                            </div>
                        </article>
                         <div class="clear"></div>
                    </section>

                <section class="discografia" id="discografia">
                    <article class="view">
                        <div><b>LONGE ANDEI</b></div>
                        
                        <img src="img/capaCD.png" alt="Longe Andei">
                        
                        <ul>
                            
                            <li style="padding-top: 0;"><a href="#discografia"><img src="img/baixaMusicaC.png" alt="Baixa Musica"></a>1. REVELAÇÃO</li>
                            <li><a href="#discografia"><img src="img/baixaMusicaC.png" alt="Baixa Musica"></a>2. ALGUÉM</li>
                            <li><a href="audio/LONGE_ANDEI_ARIOZNA.rar"><img src="img/baixaMusicaC.png" alt="Baixa Musica"></a>3. LONGE ANDEI</li>
                            <li><a href="#discografia"><img src="img/baixaMusicaC.png" alt="Baixa Musica"></a>4. TERCEIRO DIA</li>
                            <li><a href="#discografia"><img src="img/baixaMusicaC.png" alt="Baixa Musica"></a>5. BLUES SONG</li>
                            <li><a href="#discografia"><img src="img/baixaMusicaC.png" alt="Baixa Musica"></a>6. JESUS É A SALVAÇÃO</li>
                            <li><a href="#discografia"><img src="img/baixaMusicaC.png" alt="Baixa Musica"></a>7. EM ESPÍRITO E EM VERDADE</li>
                            <li><a href="#discografia"><img src="img/baixaMusicaC.png" alt="Baixa Musica"></a>8. TEU AMOR</li>
                            
                        </ul>
                        
                        <span>
                            <b>AGRADECIMENTO</b><br><br>
                            AGRADECEMOS A DEUS POR TODOS ESTES ANOS EM QUE TEM NOS SUSTENTADO, AOS NOSSOS FAMILIARES, AO FUNDADOR DESTE PROJETO NOSSO AMIGO E IRMÃO EM CRISTO THIAGO RODRIGUES (FININHO), A IGREJA ASSEMBLEIA DE DEUS (LÍRIO DOS VALES) QUE NOS APOIOU NO INÍCIO. AO SELO INDEPENDENTE ?MAIS ALÉM MUSIC GROUP? POR NOS APOIAR E A TODOS OS QUE INTERCEDEM POR NÓS.

                        </span>
                        
                    </article>
                    <div class="clear"></div>
                </section>

                <section class="videos" id="videos">
                        <article class="view">
                            <div class="div-legend"><b>VÍDEOS</b></div>
                            
                            <div style="margin:30px auto;max-width:850px;">
    
                                            <!-- Insert to your webpage where you want to display the slider -->
                                            <div id="amazingslider-1" style="display:block;position:relative;margin:16px auto 56px;">
                                                <ul class="amazingslider-slides" style="display:none;">
                                                                <li><img src="slide/images/capaCD.png" alt="ARIZONA - Longe Andei Part. ProfetaMc (Single)[2014]" />
                                                                    <video preload="none" src="http://www.youtube.com/embed/190G0cYjIqY" ></video>
                                                                </li>
                                                                    <li>
                                                                        <img src="slide/images/p7itHpG3d-s.jpg" alt="ARIZONA - Nota Oficial" />
                                                                        <video preload="none" src="http://www.youtube.com/embed/p7itHpG3d-s" ></video>
                                                                    </li>
                                                                <li>
                                                                    <img src="slide/images/gQCa8n6tsSE.jpg" alt="ARIZONA - Longe Andei Part. ProfetaMc (WEB VÍDEO ACÚSTICO)" />
                                                                    <video preload="none" src="http://www.youtube.com/embed/gQCa8n6tsSE" ></video>
                                                                </li>
                                                </ul>
                                                <ul class="amazingslider-thumbnails" style="display:none;">
                                                    <li><img src="slide/images/capaCD-tn.png" alt="Arizona" /></li>
                                                    <li><img src="slide/images/p7itHpG3d-s-tn.jpg" alt="Arizona" /></li>
                                                    <li><img src="slide/images/gQCa8n6tsSE-tn.jpg" alt="Arizona" /></li>
                                                </ul>
                                                <div class="amazingslider-engine" style="display:none;"><a href="http://amazingslider.com">JavaScript Slider</a></div>
                                            </div>
                                            <!-- End of body section HTML codes -->

                            </div>
                            <div class="clear"></div>
                        </article>
                    </section>

                <section class="imagens" id="imagens">
                    <article class="view">
                        
                        <!-- Insert to your webpage before the </head> -->
                        <script src="img/imagens/sliderengine/jquery.js"></script>
                        <script src="img/imagens/sliderengine/amazingslider.js"></script>
                        <script src="img/imagens/sliderengine/initslider-1.js"></script>
                        <!-- End of head section HTML codes -->
                        
                        <div class="div-legend-f"><b>FOTOS</b></div>
                        <div style="margin:30px auto;max-width:850px;">
    
                        <!-- Insert to your webpage where you want to display the slider -->
                        <div id="amazingslider-2" style="display:block;position:relative;margin:16px auto 56px;">
                            <ul class="amazingslider-slides" style="display:none;">
                                
                                <li><img src="img/imagens/images/Untitled-1.png" alt="Arizona" /></li>
                                <li><img src="img/imagens/images/Untitled-2.png" alt="Arizona" /></li>
                                <li><img src="img/imagens/images/Untitled-3.png" alt="Arizona" /></li>
                                <li><img src="img/imagens/images/Untitled-4.png" alt="Arizona" /></li>
                                <li><img src="img/imagens/images/Untitled-5.png" alt="Arizona" /></li>
                                <li><img src="img/imagens/images/Untitled-6.png" alt="Arizona" /></li>
                                <li><img src="img/imagens/images/Untitled-7.png" alt="Arizona" /></li>
                                <li><img src="img/imagens/images/Untitled-8.png" alt="Arizona" /></li>
                                <li><img src="img/imagens/images/Untitled-9.png" alt="Arizona" /></li>
                                <li><img src="img/imagens/images/Untitled-10.png" alt="Arizona" /></li>
                                <li><img src="img/imagens/images/Untitled-11.png" alt="Arizona" /></li>
                                <li><img src="img/imagens/images/Untitled-12.png" alt="Arizona" /></li>
                                <li><img src="img/imagens/images/Untitled-13.png" alt="Arizona" /></li>
                                <li><img src="img/imagens/images/Untitled-14.png" alt="Arizona" /></li>
                                <li><img src="img/imagens/images/Untitled-15.png" alt="Arizona" /></li>
                                <li><img src="img/imagens/images/Untitled-17.png" alt="Arizona" /></li>
                                <li><img src="img/imagens/images/Untitled-16.png" alt="Arizona" /></li>
                                
                               

                            </ul>
                            <ul class="amazingslider-thumbnails" style="display:none;">
                                <li><img src="img/imagens/images/Untitled-1-tn.png" /></li>
                                <li><img src="img/imagens/images/Untitled-2-tn.png" /></li>
                                <li><img src="img/imagens/images/Untitled-3-tn.png" /></li>
                                <li><img src="img/imagens/images/Untitled-4-tn.png" /></li>
                                <li><img src="img/imagens/images/Untitled-5-tn.png" /></li>
                                <li><img src="img/imagens/images/Untitled-6-tn.png" /></li>
                                <li><img src="img/imagens/images/Untitled-7-tn.png" /></li>
                                <li><img src="img/imagens/images/Untitled-8-tn.png" /></li>
                                <li><img src="img/imagens/images/Untitled-9-tn.png" /></li>
                                <li><img src="img/imagens/images/Untitled-10-tn.png" /></li>
                                <li><img src="img/imagens/images/Untitled-11-tn.png" /></li>
                                <li><img src="img/imagens/images/Untitled-12-tn.png" /></li>
                                <li><img src="img/imagens/images/Untitled-13-tn.png" /></li>
                                <li><img src="img/imagens/images/Untitled-14-tn.png" /></li>
                                <li><img src="img/imagens/images/Untitled-15-tn.png" /></li>
                                <li><img src="img/imagens/images/Untitled-17-tn.png" /></li>
                                <li><img src="img/imagens/images/Untitled-16-tn.png" /></li>
	                        
                                

                            </ul>
                            <div class="amazingslider-engine" style="display:none;"><a href="http://amazingslider.com">JavaScript Image Slider</a></div>
                        </div>
                        <!-- End of body section HTML codes -->

                        </div>
                        <div class="clear"></div>
                    </article>
                </section>

                    <section class="agenda" id="agenda">
                        <article class="view">
                            <div><b>AGENDA</b></div>
                            
                            <ul>
                                <li class="blocoA">
                                    <img style="height: 250px;" src="img/imagens/agenda/Untitled-1.png" alt="RIBEIRÃOZINHO - MA">
                                </li>
                                
                                <!--Referencia-->
                                <li class="blocoB">
                                    <img style="height: 250px;" src="img/imagens/agenda/Untitled-2.png" alt="CORRENTE - PI">
                                </li>
                                
                                <li class="blocoC">
                                    <img style="height: 250px;" src="img/imagens/agenda/Untitled-3.png" alt="BARREIRAS - BA">
                                </li>
                                
                                <li class="blocoD"> <!--Usar Float depois-->
                                    <img style="height: 250px;" src="img/imagens/agenda/Untitled-4.png" alt="IMPERATRIZ - MA">
                                </li>
                                
                                <!--Quebra de Linha-->
                                
                                <li style="clear: left;" class="blocoE">
                                    <img style="height: 250px;" src="img/imagens/agenda/Untitled-5.png" alt="IMPERATRIZ - MA">
                                </li>
                                
                                <li  class="blocoF">
                                    <img style="height: 250px;" src="img/imagens/agenda/Untitled-6.png" alt="IMPERATRIZ - MA">
                                </li>

                                
				<li  class="blocoG">
                                    <img style="height: 250px;" src="img/imagens/agenda/Untitled-7.png" alt="IMPERATRIZ - MA">
                                </li>
                                
                            </ul>
                            <div class="clear"></div>
                        </article>
                    </section>

                <section class="contato" id="contato">
                    <article class="view">
                        <div><b>CONTATO</b></div>
                        
                        <form action="index.php" method="post" name="form1" onsubmit="return checkPress(form1)">
                            
                            <ul>
                                
                                <li>
                                    <b>Nome: </b><br>
                                    <input type="text" name="nome" id="nome">
                                </li>
                                
                                    <li>
                                        <b>E-Mail: </b><br>
                                        <input type="text" name="email" id="email">
                                    </li>
                                
                                <li>
                                    <b>Telefone: </b><br>
                                    <input type="text" name="fone" id="fone">
                                </li>
                                
                                    <li>
                                        <b>Assunto: </b><br>
                                        <input type="text" name="assunto" id="assunto">
                                    </li>
                                
                                <li style="margin-bottom: 9px;">
                                    <b>Mensagem: </b><br>
                                    <textarea name="mensagem"></textarea>
                                </li>
                                
                                    <li>
                                        
                                        <input style="margin-top: 0px; border-radius: 0px;" type="submit" name="enviar" id="enviar" value="Enviar">
                                    </li>
                                
                            </ul> 
                        </form>
                        
                        <div style="float: right; padding: 0;">
                            <div style="text-align: center; padding: 0; font-size: 20px;">Ao Contratante - Download (Clique na Imagem)</div>
                            <a href="img/mapaPalco.rar"><img src="img/mapaPalco.png" alt="Mapa De Palco"></a>
                        </div>
                        
                        <div class="clear"></div>
                        
                    </article>
                </section>
        </div>
        
        
        <footer>
                <div class="footer-main view">
                    
                    <div class="footer-list">
                        <ul>
                            <li>
                                <a href="#"><img title="Ir ao topo" src="img/topo.png" alt="Topo"></a>
                            </li>
                        </ul>
                    </div>
                    
                    <div class="footer-direitos">
                        OLA MUNDO!!!!!!!!!!!!!!!!!!!!!!!!!!!!. Todos os direitos reservados.<br>Desenvolvido por <a style="font-family: sans-serif;" href="http://www.7nucleos.com">7NÚCLEOS</a>.
                    </div>
                    
                </div>
        </footer>
            
    </body>
</html>
